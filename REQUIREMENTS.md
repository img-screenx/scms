# CZ-OPENSCREEN Scientific Software Developer - test application

Use Python 3.x, Django 3.x and other libraries of your choice to create a simple web application browse collections of compounds.
You can use other free packages as well.

1 Installation
---
- All required python packages has to be installable with PIP from the requirements.txt file in the root of the repository.
- The application should be written as a Django project. The development server should be runable with `python manage.py runserver`.

2 Django models/database
---
The application will be stored in an SQLite database and will contain at least these Django models (database tables):

- Library
    - information about a collection of compounds ("name")
- Compound
    - information about the compound ("name", "SMILES", "InChIKey", "libraries")
    - any compound can belong to multiple collections
    - SMILES - one of the most common chemical structure representations
    - InChIKey - a hash of a chemical structure (of its structure in InChI format), used as a unique identifier.
- User - Django user model for authentication

3 Data management
---
- Prepare Django management command to process compound data (name, SMILES, InChIKey, libraries) from a csv file and store them in the database via Django models
- To fill your database, run this command on data/data.csv from this repository
- Prepare another command to generate images for all compounds in the database (you can generate compound images from SMILES using [RDKIT](https://www.rdkit.org/docs/GettingStartedInPython.html#drawing-molecules))
- These images will be used in the web interface of the app to depict compound structures

4 The web interface
---
- The interface should be responsive.
- For unauthenticated user, just a login page should be accessible.  
- The Django Admin (or any other free) application should be set up to allow 
  a management of the data in the above database.
- User can browse all compounds in the database, the number of compounds/page should be reasonable
- User can filter compounds by name/library

5 Distribution
---
- Make the project accessible to us as a github/bitbucket/other repository.
- Send us the URL, when you finish the assignment.
- Edit README file to describe the instalation of the application, data upload/management, as well as other important steps to run and use the application.
